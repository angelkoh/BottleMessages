package androidapps.angel.bottlemessages

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.TextView
import androidapps.angel.bottlemessages.data.entities.MessageEntity
import androidapps.angel.bottlemessages.databinding.FragmentReadBinding
import androidapps.angel.bottlemessages.domain.handlers.FirebaseHandler
import androidapps.angel.bottlemessages.domain.handlers.PreferenceHandler
import androidapps.angel.bottlemessages.domain.handlers.RoomHandler
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Main fragment to handle reading message bottles from firebase
 */
class ReadFragment : Fragment() {
    private var _binding: FragmentReadBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var firebase: FirebaseHandler
    private var room: RoomHandler? = null

    private var disposableConfig: Disposable? = null
    private var disposableGetter: Disposable? = null

    private var message: MessageEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        context?.let {
            room = RoomHandler(it)
        }

        retainInstance = true
        firebase = FirebaseHandler()

        disposableConfig?.takeIf { !it.isDisposed }?.dispose()
        firebase.getConfig()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                PreferenceHandler.setConfig(context, it)
                print("Config loaded: $it")
            }, {
                print("Error loading config from cloud: $it")
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentReadBinding.inflate(inflater, container, false)
        return binding.root
//        return inflater.inflate(R.layout.fragment_read, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let {
            if (room == null) {
                room = RoomHandler(it)
            }
        }

        val isDisposed = disposableGetter?.isDisposed ?: true
        if (isDisposed) hideProgressBar() else showProgressBar()

        doProcess()

        binding.swipe.setOnRefreshListener {
            binding.swipe.isRefreshing = false
            doProcess()
        }

        binding.tbLike.setOnCheckedChangeListener { _: CompoundButton, isChecked: Boolean ->
            message?.let { m ->
                if (m.liked != isChecked) {
                    m.liked = isChecked
                    if (isChecked) m.likes++
                    else if (m.likes > 0) m.likes--

                    setViewLike(m)

                    //update database
                    Single.fromCallable { room?.updateLike(m.id, m.liked) }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            print("update sql likes complete: $it, $m")
                        }, {
                            print("error updating likes: $it")
                        })

                    //update cloud
                    firebase.updateLikes(m)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            print("update cloud likes complete :$m")
                        }, {
                            print("error updating cloud likes: $it")
                        })

                }
            }
        }
    }

    private fun doProcess() {


        print("getting a random message...")
        disposableGetter?.takeIf { it.isDisposed }?.dispose()

        Glide.with(this).clear(binding.ivFlag)
        setText(binding.tvStatus, "Searching for a random bottle...")
        setText(binding.tvDate, "")
        showProgressBar()
        setLikeButtonVisibility(false)
        disposableGetter = firebase.getRandomMessageId(PreferenceHandler.getConfig(context))
            .map {
                val message = firebase.getChild(it)
                message.setPath(it)
                message
            }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ entity ->
                handleData(entity)
            }, { error ->

                message = null
                binding.ivFlag.apply { Glide.with(this).clear(this) }
                hideProgressBar()
                setLikeButtonVisibility(false)

                setText(
                    binding.tvStatus,
                    if (error == null) ""
                    else "Error: $error"
                )

                setText(binding.tvMessage, "No bottles found.\nTry writing one?")
                setText(binding.tvDate, "")
                print("error: $error")
            })

    }

    private fun handleData(entity: MessageEntity?) {

        entity?.let {
            this.message = entity
            //UPDATE UI
            setText(binding.tvMessage, entity.text)
            setText(binding.tvDate, entity.getDate())
            setText(binding.tvStatus, "")
            setCountryFlag(entity)
            setLikeButtonChecked(entity.liked)
            hideProgressBar()

            //update database
            if (room == null) {
                room = RoomHandler(requireContext())
            }

            room?.let { rm ->

                Single.fromCallable { rm.insert(entity) }

                    .flatMapCompletable {
                        print("updated to sql: $it ${entity.id}")
                        if (it > 0) {
                            print("${entity.id} added to sql. update cloud...")
                            entity.views++
                            firebase.updateViews(entity)
                        } else {
                            entity.liked = rm.selectLike(entity.id)
                            print("${entity.id} already exists in sql")
                            Completable.complete()
                        }
                    }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        print("update sql/cloud complete")
                        setViewLike(entity)
                        setLikeButtonChecked(entity.liked)

                    }, {
                        print("error updating sql/cloud: $it")
                        setViewLike(entity)
                    })
            }
        }
    }


    //LIKES
//=====
    private fun setLikeButtonVisibility(isVisible: Boolean) {
        if (isVisible) {
            binding.tbLike.visibility = View.VISIBLE
        } else {
            binding.tbLike.visibility = View.GONE
        }
    }

    private fun setLikeButtonChecked(isChecked: Boolean) {
        binding.tbLike.isChecked = isChecked
        setLikeButtonVisibility(true)
    }

    private fun setViewLike(entity: MessageEntity) {
        context?.let { ctx ->
            setText(
                binding.tvStatus,
                ctx.getString(R.string.views_likes, entity.views, entity.likes)
            )
        }
    }

    //PROGRESS BAR
//============
    private fun hideProgressBar() {
        binding.pbLoading.visibility = View.INVISIBLE
    }

    private fun showProgressBar() {
        binding.pbLoading.visibility = View.VISIBLE
    }

    //COUNTRY
//=======
    private fun setCountryFlag(entity: MessageEntity) {
        val country = entity.getCountry()
        binding.ivFlag.let {
            if (country.isNotEmpty()) {
                Glide.with(this)
                    .load("http://www.countryflags.io/$country/flat/64.png")
                    .into(binding.ivFlag)
            } else {
                Glide.with(this).clear(binding.ivFlag)
            }
        }
    }


    //TEXT VIEWS
//==========
    private fun setText(view: TextView?, s: String) {
        view?.let {
            view.text = s
        }
    }

    //COMPANION OBJECTS
//=================
    companion object {
        private const val TAG = "Angel: Read F"

        private fun print(s: String) {
            Log.d(TAG, s)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }// This property is only valid between onCreateView and

}
