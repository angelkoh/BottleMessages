package androidapps.angel.bottlemessages.data.sql;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import androidapps.angel.bottlemessages.data.entities.MessageEntity;

/**
 * Created by Angel on 16/3/2018.
 * <p>
 * Application Room persistent database
 */
@Database(entities = {MessageEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract AppDao messageDao();

}
