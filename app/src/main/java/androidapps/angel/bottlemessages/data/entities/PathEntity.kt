package androidapps.angel.bottlemessages.data.entities

/**
 * Created by Angel on 22/3/2018.
 * path entity for firebase
 * path is in the format /year/month/day/key
 */

class PathEntity {

    var year = "0"
    var month = "0"
    var day = "0"
    var key = ""


    override fun toString(): String {
        return "(year='$year', month='$month', day='$day', key='$key')"
    }


}