package androidapps.angel.bottlemessages.data.sql;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import androidapps.angel.bottlemessages.data.entities.MessageEntity;

/**
 * Created by Angel on 16/3/2018.
 * <p>
 * DAO for Application Room Persistence database
 */

@Dao
public interface AppDao {

    //    @Query("SELECT id FROM MessageEntity")
//    Single<List<String>> getAllIds();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    Long insertMessage(MessageEntity message);

    @Query("UPDATE MessageEntity SET liked = :isLike  WHERE id = :id")
    int updateLike(String id, boolean isLike);

    @Query("Select liked FROM MessageEntity WHERE id =:id")
    Boolean selectLike(String id);

}
