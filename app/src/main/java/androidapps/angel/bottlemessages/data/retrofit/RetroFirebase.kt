package androidapps.angel.bottlemessages.data.retrofit

import androidapps.angel.bottlemessages.data.entities.CountEntity
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.util.*

/**
 * Created by Angel on 14/3/2018.
 *
 * main accessor for firebase REST api using retrofit
 */

class RetroFirebase {

    val service: RetroFirebaseService

    init {
        val gson = GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapter(CountEntity::class.java, CountDeserializer())
                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        service = retrofit.create(RetroFirebaseService::class.java)
    }

    companion object {

        private const val BASE_URL = "https://androidtest-1a2f6.firebaseio.com"
    }
}

internal class CountDeserializer : JsonDeserializer<CountEntity> {
    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): CountEntity {
        val entity = CountEntity()

        val countType = object : TypeToken<HashMap<String, Boolean>>() {}.type

        val map = Gson().fromJson<Map<String, Boolean>>(json.toString(), countType)
        entity.data = map

        return entity
    }
}
