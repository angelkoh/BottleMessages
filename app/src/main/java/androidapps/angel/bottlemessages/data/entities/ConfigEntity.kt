package androidapps.angel.bottlemessages.data.entities

import com.google.gson.annotations.Expose

/**
 * Created by Angel on 22/3/2018.
 *
 * configuration entity (determines firebase path)
 */
class ConfigEntity {

    @Expose
    var useYear  = true
    @Expose
    var useMonth  = true
    @Expose
    var useDay  = true

    override fun toString(): String {
        return "ConfigEntity(useYear=$useYear, useMonth=$useMonth, useDay=$useDay)"
    }


}