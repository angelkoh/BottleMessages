package androidapps.angel.bottlemessages.data.entities

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * Created by Angel on 14/3/2018.
 *
 * bottle message entity
 */

@Entity
class MessageEntity {

    @PrimaryKey
    @Expose(serialize = false)
    var id: String = ""

    @Expose
    var text: String = ""
    @Expose
    var timeCreated: Long = System.currentTimeMillis()
    @Expose
    var locale: String = Locale.getDefault().toString()
    @Expose
    @Ignore
    var views: Int = 0
    @Expose
    @Ignore
    var likes: Int = 0

    @Expose(serialize = false)
    var liked: Boolean = false

    @Expose(serialize = false)
    @Ignore
    private var path: PathEntity = PathEntity()

    override fun toString(): String {
        return "MessageEntity(" +
                "($id) '$text', " +
                "$likes/$views, " +
                "locale='$locale', " +
                "timeCreated=$timeCreated)"
    }

    fun getCountry(): String {
        val s = locale.split("_")
        return if (s.size >= 2) s[1] else ""
    }

    fun getDate(): String {
        return if (timeCreated > 0) dateFormatter.format(Date(timeCreated))
        else ""
    }

    fun setPath(path: PathEntity) {
        this.path = path
        id = path.key
    }

    fun getPath() = path

    companion object {
        val dateFormatter = SimpleDateFormat("yyyy MMM dd h:mm:ss", Locale.getDefault())
    }
}
