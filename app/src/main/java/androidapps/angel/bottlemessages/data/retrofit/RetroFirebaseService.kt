package androidapps.angel.bottlemessages.data.retrofit

import androidapps.angel.bottlemessages.data.entities.ConfigEntity
import androidapps.angel.bottlemessages.data.entities.CountEntity
import androidapps.angel.bottlemessages.data.entities.MessageEntity
import androidapps.angel.bottlemessages.data.entities.PostMessageResponseEntity
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

/**
 * Created by Angel on 14/3/{yr}/{mth}.
 *
 *
 * Service for Firebase Database REST API
 */

interface RetroFirebaseService {

    @get:GET("/config.json")
    val config: Single<ConfigEntity>

    @get:GET("/message_bottles.json?shallow=true")
    val yearCount: Call<CountEntity>

    //NON RX GETTERS
    @GET("/message_bottles/{yr}/{mth}/{day}.json?shallow=true")
    fun getCount(@Path("yr") year: String,
                 @Path("mth") month: String,
                 @Path("day") day: String): Call<CountEntity>

    @GET("/message_bottles/{yr}/{mth}.json?shallow=true")
    fun getDayCount(@Path("yr") year: String,
                    @Path("mth") month: String): Call<CountEntity>

    @GET("/message_bottles/{yr}.json?shallow=true")
    fun getMonthCount(@Path("yr") year: String): Call<CountEntity>

    @GET("/message_bottles/{yr}/{mth}/{day}/{child}.json")
    fun getChild(@Path("yr") year: String,
                 @Path("mth") month: String,
                 @Path("day") day: String,
                 @Path("child") child: String): Call<MessageEntity>

    //INSERTS AND UPDATES
    @POST("/message_bottles/{yr}/{mth}/{day}.json")
    fun setMessage(@Path("yr") year: String,
                   @Path("mth") month: String,
                   @Path("day") day: String,
                   @Body message: MessageEntity): Single<PostMessageResponseEntity>

    @PUT("/message_bottles/{yr}/{mth}/{day}/{child}/views.json?print=silent")
    fun updateViews(@Path("yr") year: String,
                    @Path("mth") month: String,
                    @Path("day") day: String,
                    @Path("child") child: String,
                    @Body views: Int): Completable

    @PUT("/message_bottles/{yr}/{mth}/{day}/{child}/likes.json?print=silent")
    fun updateLikes(@Path("yr") year: String,
                    @Path("mth") month: String,
                    @Path("day") day: String,
                    @Path("child") child: String,
                    @Body likes: Int): Completable


    //    @DELETE("/message_bottles/{yr}/{mth}/{day}/{id}.json")
    //    Single<ResponseBody> deleteMessage(@Path("yr") int year,
    //                                       @Path("mth") int month,
    //                                       @Path("day") int day,
    //                                       @Path("id") String id);

}
