package androidapps.angel.bottlemessages

import android.content.Intent
import android.os.Bundle
import androidapps.angel.bottlemessages.databinding.ActivityReadBinding
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar


class ReadActivity : AppCompatActivity() {
    private lateinit var binding: ActivityReadBinding
    private lateinit var fragment: ReadFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_read)
        binding = ActivityReadBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        setSupportActionBar(binding.toolbar)

        fragment = supportFragmentManager.findFragmentById(R.id.fragment) as ReadFragment

        binding.fab.setOnClickListener {
            startActivity(Intent(this, SendActivity::class.java))
        }

        intent?.getStringExtra("KEY")?.let {
            Snackbar.make(binding.fab, "Bottle message sent: $it", Snackbar.LENGTH_LONG).show()
        }
    }
}
