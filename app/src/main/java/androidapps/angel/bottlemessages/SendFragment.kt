package androidapps.angel.bottlemessages

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidapps.angel.bottlemessages.data.entities.MessageEntity
import androidapps.angel.bottlemessages.databinding.FragmentSendBinding
import androidapps.angel.bottlemessages.domain.handlers.FirebaseHandler
import androidapps.angel.bottlemessages.domain.handlers.PreferenceHandler
import androidx.fragment.app.Fragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.Locale

/**
 * Main fragment to handle writing message bottles into firebase
 */
class SendFragment : Fragment() {
    private var _binding: FragmentSendBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private var simCountryCode: String = ""

    private lateinit var firebase: FirebaseHandler
    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retainInstance = true

        firebase = FirebaseHandler()

        //  the Telephony Manager
        activity?.let {
            simCountryCode = try {
                val telephonyManager =
                    it.getSystemService(Context.TELEPHONY_SERVICE) as (TelephonyManager)
                telephonyManager.simCountryIso.uppercase(Locale.ROOT)
            } catch (e: Exception) {
                ""
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentSendBinding.inflate(inflater, container, false)
        return binding.root
        //return inflater.inflate(R.layout.fragment_send, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val isDisposed = disposable?.isDisposed ?: true
        binding.pbLoading.visibility = if (isDisposed) View.GONE else View.VISIBLE

    }


    fun doProcess() {

        val s = binding.etMessage.text?.toString()?.trim() ?: ""
        if (s.isNotEmpty()) {

            setText(binding.tvStatus, "preparing message bottle...")
            binding.pbLoading.visibility = View.VISIBLE
            val message = MessageEntity()
            if (simCountryCode.isNotEmpty()) {
                message.locale = Locale.getDefault().language + "_" + simCountryCode
            }
            val config = PreferenceHandler.getConfig(context)


            message.text = s
            disposable?.takeIf { it.isDisposed }?.dispose()
            disposable = firebase.setChild(config, message)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        try {
                            val intent = Intent(activity, ReadActivity::class.java)
                            intent.putExtra("KEY", it.toString())
                            startActivity(intent)
                            activity?.finish()
                        }catch(e: Exception){
                            print("activity stopped. do nothing.")
                        }
                    }, {
                        setText(binding.tvStatus, "Error! Try again. (${it.message})")

                        binding.pbLoading.visibility = View.GONE
                    })

        } else {
            setText(binding.tvStatus, "Enter something")

            binding.pbLoading.visibility = View.GONE
        }
    }

    fun handleTextIntent(s: String) {
        setText(binding.etMessage, s)
    }

    private fun setText(view: TextView?, s: String) {
        view?.let {
            view.text = s
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
