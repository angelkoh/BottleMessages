package androidapps.angel.bottlemessages.domain.handlers


import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidapps.angel.bottlemessages.data.entities.ConfigEntity


/**
 * PROJECT: Flutter4d
 * angelandroidapps.twitch.flutter4d.domain.handler
 * Created by Angel on 12/6/2015.
 */
object PreferenceHandler {


    //CONFIG
    //===========
    fun setConfig(context: Context?, config: ConfigEntity) {
        context?.let {
            setUseYear(context, config.useYear)
            setUseMonth(context, config.useMonth)
            setUseDay(context, config.useDay)
        }
    }

    fun getConfig(context: Context?): ConfigEntity {
        val config = ConfigEntity()
        context?.let {
            config.useYear = getUseYear(context)
            config.useMonth = getUseMonth(context)
            config.useDay = getUseDay(context)
        }
        return config
    }

    private fun setUseYear(context: Context, value: Boolean) = setBoolean(context, "pref_use_year", value)

    private fun getUseYear(context: Context) = getBoolean(context, "pref_use_year", false)

    private fun setUseMonth(context: Context, value: Boolean) = setBoolean(context, "pref_use_month", value)

    private fun getUseMonth(context: Context) = getBoolean(context, "pref_use_month", false)

    private fun setUseDay(context: Context, value: Boolean) = setBoolean(context, "pref_use_day", value)

    private fun getUseDay(context: Context) = getBoolean(context, "pref_use_day", false)


    //BASE METHODS
    //============
    private fun getPref(context: Context): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.applicationContext)

    @SuppressLint("CommitPrefEdits")
    private fun getEditor(context: Context): SharedPreferences.Editor =
            getPref(context).edit()

    private fun setBoolean(context: Context, key: String, value: Boolean) = getEditor(context).putBoolean(key, value).commit()


    private fun getBoolean(context: Context, key: String, defaultValue: Boolean) = getPref(context).getBoolean(key, defaultValue)

}
