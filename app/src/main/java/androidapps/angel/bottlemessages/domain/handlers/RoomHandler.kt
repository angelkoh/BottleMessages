package androidapps.angel.bottlemessages.domain.handlers

import android.content.Context
import androidapps.angel.bottlemessages.data.entities.MessageEntity
import androidapps.angel.bottlemessages.data.sql.AppDatabase
import androidx.room.Room


/**
 * Created by Angel on 21/3/2018.
 *
 * handler for room sqlite database
 */

class RoomHandler(val context: Context) {

    private var mDatabase: AppDatabase = Room.databaseBuilder(context.applicationContext,
            AppDatabase::class.java, "message_bottle.db").build()

    fun insert(message: MessageEntity): Long =
            mDatabase.messageDao().insertMessage(message)

    fun updateLike(id: String, isLike: Boolean): Int =
            mDatabase.messageDao().updateLike(id, isLike)

    fun selectLike(id: String): Boolean  =
            mDatabase.messageDao().selectLike(id)

}
