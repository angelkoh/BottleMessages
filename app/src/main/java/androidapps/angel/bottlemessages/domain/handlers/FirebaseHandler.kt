package androidapps.angel.bottlemessages.domain.handlers

import android.util.Log
import androidapps.angel.bottlemessages.data.entities.*
import androidapps.angel.bottlemessages.data.retrofit.RetroFirebase
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import io.reactivex.Completable
import io.reactivex.Single
import java.util.*


/**
 * Created by Angel on 14/3/2018.
 *
 * handler for firebase database.
 * combines REST API and Android API
 */

class FirebaseHandler {

    private var database = FirebaseDatabase.getInstance()
    private var baseReference: DatabaseReference
    private var retroFirebase: RetroFirebase
    private val calendar: Calendar

    init {
        baseReference = database.getReference("messages")
        retroFirebase = RetroFirebase()
        calendar = Calendar.getInstance()
    }

    fun getConfig(): Single<ConfigEntity> =
            retroFirebase.service.config

    fun getChild(path: PathEntity): MessageEntity =
            retroFirebase.service.getChild(
                    path.year,
                    path.month,
                    path.day,
                    path.key
            ).execute().body()!!

    fun getRandomMessageId(configEntity: ConfigEntity?): Single<PathEntity> {

        return Single.fromCallable({
            val path = PathEntity()
            val service = retroFirebase.service

            configEntity?.let {
                if (configEntity.useYear) {
                    path.year = getRandom(service.yearCount
                            .execute().body()!!)
                    print("Year  $path")
                    if (configEntity.useMonth) {
                        path.month = getRandom(service.getMonthCount(path.year)
                                .execute().body()!!)
                        print("Month $path")
                        if (configEntity.useDay) {
                            path.day = getRandom(service.getDayCount(path.year, path.month)
                                    .execute().body()!!)
                            print("Day  $path")
                        }
                    }
                }
            }
            path.key = getRandom(service.getCount(path.year, path.month, path.day)
                    .execute().body()!!)

            print("key: $path")
            path
        })
    }


    //SETTERS
    //=======
    fun setChild(configEntity: ConfigEntity?, message: MessageEntity): Single<PostMessageResponseEntity> {
        return setChild(updatePath(configEntity, message))
    }

    private fun setChild(message: MessageEntity): Single<PostMessageResponseEntity> {

        return retroFirebase.service.setMessage(
                message.getPath().year,
                message.getPath().month,
                message.getPath().day,
                message)
    }

    private fun updatePath(configEntity: ConfigEntity?, message: MessageEntity): MessageEntity {

        message.setPath(getPath(configEntity, message.timeCreated))
        return message
    }

    private fun getPath(configEntity: ConfigEntity?, time: Long): PathEntity {

        calendar.timeInMillis = time
        val path = PathEntity()
        configEntity?.let { c ->
            if (c.useYear) {
                path.year = calendar.get(Calendar.YEAR).toString()
                if (c.useMonth) {
                    path.month = calendar.get(Calendar.MONTH).toString()
                    if (c.useDay) {
                        path.day = calendar.get(Calendar.DAY_OF_MONTH).toString()
                    }
                }
            }
        }

        return path
    }

    //UPDATES
    //=======
    fun updateViews(message: MessageEntity): Completable {

        return if (message.id.isNotEmpty()) {

            retroFirebase.service.updateViews(
                    message.getPath().year,
                    message.getPath().month,
                    message.getPath().day,
                    message.id,
                    message.views)

        } else {
            Completable.error(Throwable("unknown message ID"))
        }
    }


    fun updateLikes(message: MessageEntity): Completable {

        return if (message.id.isNotEmpty()) {

            retroFirebase.service.updateLikes(
                    message.getPath().year,
                    message.getPath().month,
                    message.getPath().day,
                    message.id,
                    message.likes)

        } else {
            Completable.error(Throwable("unknown message ID"))
        }
    }


    //UTILS
    private fun getRandom(it: CountEntity): String {
        return it.data?.takeIf { it.isNotEmpty() }
                ?.keys?.shuffled()?.first() ?: "0"
    }


    companion object {
        private const val TAG = "Angel: Firebase H"

        private fun print(s: String) {
            Log.d(TAG, s)
        }
    }
}
