package androidapps.angel.bottlemessages

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidapps.angel.bottlemessages.databinding.ActivitySendBinding
import androidx.appcompat.app.AppCompatActivity

class SendActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySendBinding
    private var fragment: SendFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_send)
        binding = ActivitySendBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setSupportActionBar(binding.toolbar)

        fragment = supportFragmentManager.findFragmentById(R.id.fragment) as SendFragment

        binding.fab.setOnClickListener {
            fragment?.doProcess()
            hideKeyboard()
        }


        intent?.getStringExtra(Intent.EXTRA_TEXT)?.let {
            fragment?.handleTextIntent(it)
        }

    }

    private fun hideKeyboard() {
        this.currentFocus?.let {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        print("onNewIntent()")
        setIntent(intent)

        intent?.getStringExtra(Intent.EXTRA_TEXT)?.let {
            fragment?.handleTextIntent(it)
        }

    }


    companion object {
        private const val TAG = "Angel: Send A"

        private fun print(s: String) {
            Log.d(TAG, s)
        }
    }

}
