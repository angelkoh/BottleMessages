package androidapps.angel.bottlemessages.data.sql;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public final class AppDatabase_Impl extends AppDatabase {
  private volatile AppDao _appDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `MessageEntity` (`id` TEXT NOT NULL, `text` TEXT NOT NULL, `timeCreated` INTEGER NOT NULL, `locale` TEXT NOT NULL, `liked` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"8378a1f13d3230925a96b64b37c959db\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `MessageEntity`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsMessageEntity = new HashMap<String, TableInfo.Column>(5);
        _columnsMessageEntity.put("id", new TableInfo.Column("id", "TEXT", true, 1));
        _columnsMessageEntity.put("text", new TableInfo.Column("text", "TEXT", true, 0));
        _columnsMessageEntity.put("timeCreated", new TableInfo.Column("timeCreated", "INTEGER", true, 0));
        _columnsMessageEntity.put("locale", new TableInfo.Column("locale", "TEXT", true, 0));
        _columnsMessageEntity.put("liked", new TableInfo.Column("liked", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysMessageEntity = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesMessageEntity = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoMessageEntity = new TableInfo("MessageEntity", _columnsMessageEntity, _foreignKeysMessageEntity, _indicesMessageEntity);
        final TableInfo _existingMessageEntity = TableInfo.read(_db, "MessageEntity");
        if (! _infoMessageEntity.equals(_existingMessageEntity)) {
          throw new IllegalStateException("Migration didn't properly handle MessageEntity(androidapps.angel.bottlemessages.data.entities.MessageEntity).\n"
                  + " Expected:\n" + _infoMessageEntity + "\n"
                  + " Found:\n" + _existingMessageEntity);
        }
      }
    }, "8378a1f13d3230925a96b64b37c959db", "fd2651e78b439ffbf819ed8f61e9ab1c");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "MessageEntity");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `MessageEntity`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public AppDao messageDao() {
    if (_appDao != null) {
      return _appDao;
    } else {
      synchronized(this) {
        if(_appDao == null) {
          _appDao = new AppDao_Impl(this);
        }
        return _appDao;
      }
    }
  }
}
